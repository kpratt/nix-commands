#include <iostream>
#include <fstream>
#include <algorithm>

using namespace std;

int main(int argc, char** argv){

  char buff [4096];
  size_t count = atoi(argv[1]);
  size_t read = 0;
  istream *in;

  if(argc > 2){
    in = new ifstream(argv[2]);
  } else {
    in = &cin;
  }

 
  while(read < count) {
    (*in).read(buff, std::min(count-read, (size_t)4096));
    read += (*in).gcount();
  }
  
  while( ! (*in).eof() ) {
    (*in).read(buff, 4096);
    read = (*in).gcount();
    cout.write(buff, read);
  }
  cout.flush();
  
  return 0;

}


