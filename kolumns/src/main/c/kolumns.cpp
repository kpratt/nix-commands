#include <iostream>
#include <list>
#include <string>
#include <stdlib.h>
#include <string.h>
#include <algorithm>
#include <functional>
#include <cctype>
#include <locale>
#include <iomanip>

using namespace std;

// trim from start
static inline std::string &ltrim(std::string &s) {
    s.erase(s.begin(), std::find_if(s.begin(), s.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
    return s;
}

// trim from end
static inline std::string &rtrim(std::string &s) {
    s.erase(std::find_if(s.rbegin(), s.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
    return s;
}

// trim from both ends
static inline std::string &trim(std::string &s) {
    return ltrim(rtrim(s));
}

bool isNumChar(char x)
{
    return (x >= 48 && x <= 57) || x == 46;
}
bool is_digits(const std::string &str)
{
    return isdigit(str.front()) && isdigit(str.back()) && std::all_of(str.begin(), str.end(), isNumChar); // C++11
}

int main (int argc, char** args){
    size_t column_count = 1;
    size_t* column_widths = (size_t*)malloc(sizeof(size_t));
    list<string> substrings;
    size_t i;
    string response;
    size_t start;
    size_t end = 0;
    column_widths[0] = 0;

    getline(cin, response);
    while(cin){
        response = trim(response);
        start = end = 0;

        i=0;
        while(start != -1 && start < response.size() && end < response.size()) {
            if(i >= column_count) {
                column_count *= 2;
                column_widths = (size_t*)realloc(column_widths, column_count * sizeof(size_t));
                bzero(&column_widths[i], sizeof(size_t) * (column_count - i));
            }
            end = response.find_first_of(" \t", start+1);
            end = min(end, response.size());
            column_widths[i] = max(column_widths[i], end-start+2);
            substrings.push_back(response.substr(start, end-start));
            start = end+1;
            i++;
        }

        i=0;
        for(auto iter = substrings.begin(); iter != substrings.end() && i<100 ; iter++ ) {
	  if(is_digits(*iter)){
        cout << " "
          << setw(column_widths[i])
          << std::right
		  << std::setprecision(3)
		  << std::fixed
		  << stod(trim(*iter))
		  << " ";
	  } else {
	    cout << " "
		 << setw(column_widths[i])
		 << std::left
		 << *iter
		 << " "
		 ;
	  }
	  
            i++;
        }
        cout << endl;

        substrings.clear();
        getline(cin, response);
    }

    free(column_widths);
}
